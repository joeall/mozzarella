<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);


	if(isset($_GET['id']) && !empty($_GET['id']) && ctype_digit($_GET['id'])) {
		$id = $_GET['id'];
	}
	else {
		header("Location:index.php");
		die();
	}
	
	$locale = "en-US";
	if(isset($_SESSION["lang"]) && !empty($_SESSION["lang"]))
		$locale = $_SESSION["lang"];
	$sql = "SELECT * FROM(
	SELECT * FROM extension_locale
	INNER JOIN extensions USING (ext_id)
	INNER JOIN licenses USING (lic_id)
	WHERE ext_id = :id
	AND (locale = :locale or locale = 'en-US')
	GROUP by ext_id, locale
	ORDER by FIELD(locale, :locale, 'en-US') ASC)
	AS c GROUP BY ext_id";
	$stmt=$db->prepare($sql);
	$stmt->bindValue(":locale", $locale);
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();
	$extension = $stmt->fetch(PDO::FETCH_ASSOC);

	$sql = "SELECT * FROM (
	SELECT preview_locale.*
	FROM preview_locale
	WHERE ext_id = :id
	AND (locale = :locale or locale = 'en-US')
	GROUP BY img_id, locale
	ORDER BY FIELD(locale, :locale, 'en-US') ASC)
	AS c GROUP BY img_id";
	$stmt=$db->prepare($sql);
	$stmt->bindValue(":locale", $locale);
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();
	$previews = $stmt->fetchall(PDO::FETCH_ASSOC);

	if(!$extension)
	{
		header("Location:index.php");
		die();
	}



	// get locales list
	$stmt = $db->prepare("SELECT locale FROM extension_locale
	WHERE ext_id = :id");
	$stmt->bindValue(":id", $id, PDO::PARAM_INT);
	$stmt->execute();
	$locales = $stmt->fetchAll(PDO::FETCH_ASSOC);

	// print_r($locales);

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">

	<style type="text/css">
		
		#main {
			max-width: 1366px;
			margin: auto;
			padding: 10px;
			background: white;
		}
		#content{
			padding: 20px;
		}
		h2+p {
			margin-bottom: 10px;
		}
		#dlBtn {
			display: inline-flex;
			align-items: center;
			padding: 10px;
			background: blueviolet;
			color: white;
			text-decoration: none;
			margin-top: 10px;
		}
		#dlBtn img {
			margin-right: 5px;
		}
		.thumbnail {
			padding: 10px;
			max-width: 400px;
			height: fit-content;
		}
	</style>
</head>
<body>
	<?php include("includes/header.php"); ?>

	<h1><?=$extension["name"]?></h1>


	
	<div id="main">
	<div id="content">
		<p class=""><?=nl2br($extension["description"]) ?? '<i>No description</i>'?></p>
		
		<a id="dlBtn" href="<?=$extension["download_link"]?>"><img src="assets/install.png">Install</a>
		<ul>
			<li>License: <a href="<?=$extension["license_url"]?>" target="_blank"><?=$extension["lic_name"]?></a> </li>
			<li>Weekly downloads: <?=$extension["weekly_downloads"]?></li>
			<li>Average daily users: <?=$extension["average_daily_users"]?></li>
			<li>Rating: <?=$extension["average_rating"]?>/5 of <?=$extension["ratings_count"]?> ratings</li>
			<li>Created: <?=$extension["created"]?></li>
			<li>Last updated: <?=$extension["last_updated"]?></li>
			<li>Homepage: <a href="<?=$extension["homepage"]?>"><?=$extension["homepage"]?></a></li>
			<li>Support <a href="<?=$extension["support_url"]?>">site</a> and <a href="mailto:<?=$extension["support_email"]?>">email</a></li>
			<?php if ($extension["contributions_url"]){?>
			<li><a href="<?=$extension["contributions_url"]?>">Donate</a></li>
			<?php } ?>
			<li>Orig: <a href="<?=$extension["url"]?>"><?=$extension["url"]?></a></li>
			<li>API: <a href="https://addons.mozilla.org/api/v5/addons/addon/<?=$extension["guid"]?>"><?=$extension["guid"]?></a></li>
		</ul>
	<?php if(count($previews) > 1) { ?>
	<div id="thumbnails" class="fluid">
	<?php	foreach($previews as $preview) {
		$path="images/previews/".substr($preview["img_id"],0,3)."/".$preview["img_id"]."_thumb.png";?>
		<img class="thumbnail" src="<?=$path?>" alt="<?=$preview["caption"]?>">
	<?php }	?>
	</div>
	<?php } ?>
	</div>
</div>
	
	<?php include("includes/footer.php"); ?>
</body>
</html>
