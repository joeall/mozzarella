# Mozzarella

A website that displays free addons for Mozilla Firefox and derivative browsers.

cron.py fetches the addons from the addons.mozilla.org API and stores them into a MySQL Database.

# Installation

Tested with PHP 8.1, MySQL 15.1, Apache 2.4.52

First apply the mozzarella.sql database schema.

Running cron.py will add items to the database.

To run the main website, go to index.php

## License

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Project status

The project is built during my free time. Feel free to contribute to it.

# screenshots


![image-1.png](./image-1.png)

![image-2.png](./image-2.png)

![image.png](./image.png)
