<?php
/*
 *  Copyright (C) 2022,2023 Joey Allard
 *  Copyright (C) 2023 Ruben Rodriguez <ruben@gnu.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

	require_once("common.php");

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);


	// sets the default display language to "en-US" locale
	// todo allow user to switch language and display according extension name and description (or default)
	// todo: because it is storing a cookie (functional only), need check regulations (RGPD..)

	// not implemented yet
	// $_SESSION['locale'] = "en-US"; 

	if(isset($_GET['id']) && !empty($_GET['id'])) {
		$id = $_GET['id'];
	}
	else {
		header("Location:index.php");
		die();
	}

	$page = 1;
	if(isset($_GET['page']) && !empty($_GET['page']) && ctype_digit($_GET['page'])) {
		$page = $_GET['page'];
	}

	$offset = $page * PER_PAGE - PER_PAGE;

	$stmt = $db->prepare("SELECT count(*) c FROM ext_cat WHERE cat_id = :cat_id ");
	$stmt->bindValue(":cat_id", $id, PDO::PARAM_STR);
	$stmt->execute();
	$count = $stmt->fetch(PDO::FETCH_ASSOC)['c'];

	

	// get the extensions from this category, on the current page

	// selecting the name for current display locale if available

	$stmt = $db->prepare("SELECT * FROM (
			SELECT * FROM ext_cat
			INNER JOIN extensions USING (ext_id)
			INNER JOIN categories USING (cat_id)
			INNER JOIN extension_locale USING (ext_id)
			WHERE cat_id = :cat_id
			AND (locale = :locale or locale = 'en-US')
			GROUP BY ext_id, locale
			ORDER BY average_daily_users DESC, name DESC, FIELD(locale, :locale, 'en-US') ASC
			LIMIT :l OFFSET :o) AS c GROUP BY ext_id
			ORDER BY average_daily_users DESC, name DESC");
	$stmt->bindValue(":cat_id", $id, PDO::PARAM_STR);
	$stmt->bindValue(":locale", $_SESSION['lang']);
	$stmt->bindValue(":l", PER_PAGE, PDO::PARAM_INT);
	$stmt->bindValue(":o", $offset, PDO::PARAM_INT);
	$stmt->execute();
	$extensions = $stmt->fetchAll(PDO::FETCH_ASSOC);


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="stylesheet" type="text/css" href="fontawesome-free-6.4.0-web/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="main.css">
	<style type="text/css">
		#container {
			display: flex;
			max-width: 1366px;
			margin: 10px auto;

		}
		#left {
			background: orange;
			background: white;
			margin-right: 10px;
			padding: 10px;
			display: none;
		}
		#main {
			background: white;
			flex: 1;
			padding: 10px;
			display: flex;
			flex-direction: column;
		}
		
		
		

		@media screen and (max-width: 1000px)
		{
			#container {
				flex-direction: column;
			}

			background: red;
		}
		#left {
			background: orange;

		}
	</style>
</head>
<body>
	<?php include("includes/header.php"); ?>

	<h1><?=$count?> apps in <?=$extensions[0]["cat_id"]?></h1>

	<div id="container">
		<div id="left">
			a
		</div>
		<div id="main">
			<?php 
				if($extensions) {
					foreach($extensions as $e)
					{
						?>
						<div class="extension">
							<img src="images/icons/<?=$e["ext_id"]?>.png">
							<div class="extensionRight">
								<a href="extension.php?id=<?=$e["ext_id"]?>"><?=$e["name"]?></a>
								<p><?=nl2br(strip_tags($e["summary"] ))?></p>
							</div>
						</div>
						<?php
					}
				}

				?>
				<div id="pagination">
					
					<?php

						if($page > 1)
						{
							?>
							<a class="pagination" href="category.php?id=<?=$id?>&page=<?=$page-1?>">Previous</a>
							<?php
						}
						if($page < ceil($count / PER_PAGE))
						{
							?>
							<a class="pagination" href="category.php?id=<?=$id?>&page=<?=$page+1?>">Next</a>
							<?php
						}

					?>

				</div>
				



		</div>
	</div>

	<?php include("includes/footer.php"); ?>
	
</body>
</html>
